shader_type canvas_item;
render_mode unshaded;

uniform float blur : hint_range(0, 1.0, 0.0001) = 0.0;


void fragment() {
	float samples = 6.0; // MUST BE A MULTIPLE OF 2
	vec4 color = vec4(0.0);

	for (float i = 1.0; i <= samples / 2.0; i++) {
		color += texture(TEXTURE, UV + (i * vec2(0.0, blur) ) / samples);
		color += texture(TEXTURE, UV - (i * vec2(0.0, blur) ) / samples);
	}
	COLOR = color / samples;
}